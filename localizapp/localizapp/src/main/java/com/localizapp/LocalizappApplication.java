package com.localizapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocalizappApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocalizappApplication.class, args);
	}

}
